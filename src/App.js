import './App.css';

import { useState } from 'react';

import * as yup from 'yup';
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';

import axios from 'axios';

import Logo from './assets/logo.png';

function App() {

  const [reqMessage, setReqMessage] = useState('');

  const formSchema = yup.object().shape({
    email: yup.string(),
    password: yup.string(),
  });

  const { register, handleSubmit } = useForm({
    resolver: yupResolver(formSchema),
  });

  const handleForm = (data) => {
    axios
      .post('https://kenziehub.me/sessions/', data)
      .then((response) => {
        setReqMessage('Requisição completa');
      })
      .catch((err) => {
        setReqMessage('Requisição falhou');
      });
  }

  return (
    <div className="App">
      <header className="App-header">
        <div className="container">
          <div className="logo">
            <p>Connector</p>
            <img src={Logo} alt='logo da connectorAPI'/>
          </div>
          <form onSubmit={handleSubmit(handleForm)}>
            <div className="input-container">
              <input type="text" placeholder="Digite seu email aqui" {...register('email')}/>
            </div>
            <div className="input-container">
              <input type="password" placeholder="Digite sua senha aqui" {...register('password')}/>
            </div> 
            <button type="submit" className="btn-form"></button>
          </form>
        </div>
        <p className="req-message">{reqMessage}</p>
      </header>
    </div>
  );
}

export default App;
